package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func printSubMenu() {
	println("1. Изменить фамилию")
	println("2. Изменить имя")
	println("3. Изменить отчество")
	println("4. Изменить группу")
	println("5. Добавить оценку")
	println("6. Изменить оценку")
	println("7. Удалить оценку")
	println("8. Назад")
}

func changeLastName(s *Student) {
	fmt.Print("Введите новую фамилию студента> ")
	fmt.Scan(&s.lastName)
}

func changeFirstName(s *Student) {
	fmt.Print("Введите новое имя студента> ")
	fmt.Scan(&s.firstName)
}

func changeMiddleName(s *Student) {
	fmt.Print("Введите новое отчество студента> ")
	fmt.Scan(&s.middleName)
}

func changeGroup(s *Student) {
	fmt.Print("Введите новую группу студента> ")
	fmt.Scan(&s.group)
}

func addMark(s *Student) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Введите название предмета> ")
	subj, _ := reader.ReadString('\n')
	subj = strings.TrimSuffix(subj, "\n")

	if _, ok := s.marks[subj]; ok {
		fmt.Println("Оценка по этому предмету уже существует")
		return
	}

	var m int

	fmt.Print("Введите оценку> ")
	fmt.Scan(&m)

	s.marks[subj] = m
}

func changeMark(s *Student) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Введите название предмета> ")
	subj, _ := reader.ReadString('\n')
	subj = strings.TrimSuffix(subj, "\n")

	if _, ok := s.marks[subj]; !ok {
		fmt.Println("Оценки по этому предмету не существует")
		return
	}

	var m int

	fmt.Print("Введите оценку> ")
	fmt.Scan(&m)

	s.marks[subj] = m
}

func delMark(s *Student) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Введите название предмета> ")
	subj, _ := reader.ReadString('\n')
	subj = strings.TrimSuffix(subj, "\n")

	if _, ok := s.marks[subj]; ok {
		var ans string

		fmt.Printf("Вы уверены, что хотите удалить предмет \"%v\"? [y/n] ", subj)
		fmt.Scanln(&ans)

		ans = strings.ToLower(ans)

		if ans == "y" || ans == "yes" {
			delete(s.marks, subj)
		}
	} else {
		fmt.Println("Такого предмета не существует")
	}
}

var subMenuItems = []func(*Student) {
	changeLastName,
	changeFirstName,
	changeMiddleName,
	changeGroup,
	addMark,
	changeMark,
	delMark,
}

func subMenuLoop(students *[]Student) {
	if len(*students) == 0 {
		fmt.Println("Студентов нет")
		return
	}

	for i, v := range *students {
		v.PrintBrief(i + 1)
	}

	var n int

	fmt.Print("Номер студента для редактирования> ")
	fmt.Scan(&n)

	n -= 1

	for n < 0 || n >= len(*students) {
		fmt.Print("Такого номера студента не существует\n> ")
		fmt.Scan(&n)
		n -= 1
	}

	currentStudent := (*students)[n]

	running := true

	for running {
		fmt.Println("Редактируемый студент:")
		currentStudent.PrintDetailed(1)
		fmt.Println()

		printSubMenu()

		fmt.Print("> ")
		fmt.Scan(&n)

		n -= 1

		if n < 0 || n > len(subMenuItems) {
			fmt.Println("Некорректный номер пункта меню")
		} else if n == len(subMenuItems) {
			running = false
		} else {
			subMenuItems[n](&currentStudent)

			err := saveData(students)
			if err != nil {
				fmt.Println("Ошибка сохранения данных. Продолжайте на свой страх и риск!")
			}
		}
	}
}
