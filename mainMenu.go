package main

import (
	"fmt"
	"strings"
)

func printMainMenu() {
	println("1. Список студентов")
	println("2. Добавить студента")
	println("3. Редактировать студента")
	println("4. Удалить студента")
	println("5. Показать отличников")
	println("6. Показать двоечников")
	println("7. Выход")
}

func listStudents(students *[]Student) {
	if len(*students) == 0 {
		fmt.Println("Студентов нет")
	} else {
		for i, s := range *students {
			s.PrintDetailed(i + 1)
		}
	}
}

func addStudent(students *[]Student) {
	var s Student

	fmt.Print("Фамилия: ")
	fmt.Scan(&s.lastName)
	fmt.Print("Имя: ")
	fmt.Scan(&s.firstName)
	fmt.Print("Отчество: ")
	fmt.Scan(&s.middleName)
	fmt.Print("Группа: ")
	fmt.Scan(&s.group)
	s.marks = make(map[string] int)

	*students = append(*students, s)

	err := saveData(students)
	if err != nil {
		fmt.Println("Ошибка сохранения данных. Продолжайте на свой страх и риск!")
	}
}

func editStudent(students *[]Student) {
	subMenuLoop(students)
}

func delStudent(students *[]Student) {
	if len(*students) == 0 {
		fmt.Println("Студентов нет")
		return
	}

	for i, s := range *students {
		s.PrintBrief(i + 1)
	}

	var n int

	fmt.Print("Номер студента для удаления> ")
	fmt.Scan(&n)

	n -= 1

	for n < 0 || n >= len(*students) {
		fmt.Print("Такого номера студента не существует\n> ")
		fmt.Scan(&n)
		n -= 1
	}

	s := (*students)[n]

	var ans string

	fmt.Printf("Уверены ли вы, что хотите удалить студента %v? [y/n] ", &s)
	fmt.Scan(&ans)

	ans = strings.ToLower(ans)

	if ans == "y" || ans == "yes" {
		*students = append((*students)[:n], (*students)[n+1:]...)

		err := saveData(students)
		if err != nil {
			fmt.Println("Ошибка сохранения данных. Продолжайте на свой страх и риск!")
		}
	}
}

func showExcellentStudents(students *[]Student) {
	hasExcellent := false
	for i, s := range *students {
		if s.IsExcellent() {
			s.PrintDetailed(i + 1)
			hasExcellent = true
		}
	}

	if !hasExcellent {
		fmt.Println("Отличников нет")
	}
}

func showBadStudents(students *[]Student) {
	hasBad := false
	for i, s := range *students {
		if s.IsBad() {
			s.PrintDetailed(i + 1)
			hasBad = true
		}
	}

	if !hasBad {
		fmt.Println("Двоечников нет")
	}
}

var mainMenuItems = []func(*[]Student) {
	listStudents,
	addStudent,
	editStudent,
	delStudent,
	showExcellentStudents,
	showBadStudents,
}

func mainMenuLoop(students *[]Student) {
	var n int

	running := true

	for running {
		printMainMenu()

		fmt.Print("> ")
		fmt.Scan(&n)

		n -= 1

		if n < 0 || n > len(mainMenuItems) {
			fmt.Println("Некорректный номер пункта меню")
		} else if n == len(mainMenuItems) {
			running = false
		} else {
			mainMenuItems[n](students)
		}
	}
}
