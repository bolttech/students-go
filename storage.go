package main

import (
	"encoding/binary"
	"os"
)

const filename = "students.dat"

func readString(where *os.File, buf *[]byte) (string, error) {
	_, err := where.Read(*buf)
	if err != nil {
		return "", err
	}
	size := binary.LittleEndian.Uint32(*buf)

	result := make([]byte, size)

	_, err = where.Read(result)
	if err != nil {
		return "", nil
	}

	return string(result), nil
}

func loadData(students *[]Student) error {
	f, err := os.Open(filename)
	if err != nil {
		return nil // игнорируем, если не смогли прочитать
	}

	defer f.Close()

	num_buf := make([]byte, 4)

	_, err = f.Read(num_buf)
	if err != nil {
		return err
	}

	count := int(binary.LittleEndian.Uint32(num_buf))

	for i := 0; i < count; i++ {
		var student Student

		student.lastName, err = readString(f, &num_buf)
		if err != nil {
			return err
		}

		student.firstName, err = readString(f, &num_buf)
		if err != nil {
			return err
		}

		student.middleName, err = readString(f, &num_buf)
		if err != nil {
			return err
		}

		student.group, err = readString(f, &num_buf)
		if err != nil {
			return err
		}

		student.marks = make(map[string] int)

		_, err = f.Read(num_buf)
		if err != nil {
			return err
		}

		size := int(binary.LittleEndian.Uint32(num_buf))

		for j := 0; j < size; j++ {
			subj, err := readString(f, &num_buf)
			if err != nil {
				return err
			}

			mark := make([]byte, 1)
			_, err = f.Read(mark)
			if err != nil {
				return err
			}

			student.marks[subj] = int(mark[0])
		}

		*students = append(*students, student)
	}

	return nil
}

func writeString(where *os.File, what string, buf *[]byte) error {
	binary.LittleEndian.PutUint32(*buf, uint32(len(what)))
	_, err := where.Write(*buf)
	if err != nil {
		return err
	}

	_, err = where.Write([]byte(what))
	if err != nil {
		return err
	}

	return nil
}

func saveData(students *[]Student) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}

	defer f.Close()

	num_buf := make([]byte, 4)

	binary.LittleEndian.PutUint32(num_buf, uint32(len(*students)))

	_, err = f.Write(num_buf)
	if err != nil {
		return err
	}

	for _, student := range *students {
		err = writeString(f, student.lastName, &num_buf)
		if err != nil {
			return err
		}

		err = writeString(f, student.firstName, &num_buf)
		if err != nil {
			return err
		}

		err = writeString(f, student.middleName, &num_buf)
		if err != nil {
			return err
		}

		err = writeString(f, student.group, &num_buf)
		if err != nil {
			return err
		}

		binary.LittleEndian.PutUint32(num_buf, uint32(len(student.marks)))
		_, err = f.Write(num_buf)
		if err != nil {
			return err
		}

		for subj, mark := range student.marks {
			err = writeString(f, subj, &num_buf)
			if err != nil {
				return err
			}

			_, err = f.Write([]byte{byte(mark)})
			if err != nil {
				return err
			}
		}
	}

	return nil
}
