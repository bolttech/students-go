package main

import "fmt"

type Student struct {
	lastName, firstName, middleName string
	group string
	marks map[string] int
}

func (s *Student) PrintDetailed(n int) {
	fmt.Printf("=== %v ===\n", n)
	fmt.Printf("Фамилия: %v\n", s.lastName)
	fmt.Printf("Имя: %v\n", s.firstName)
	fmt.Printf("Отчество: %v\n", s.middleName)
	fmt.Printf("Группа: %v\n", s.group)
	fmt.Println("Оценки")
	if len(s.marks) == 0 {
		fmt.Println("  Оценок нет")
	} else {
		for k, v := range s.marks {
			fmt.Printf("  %v: %v\n", k, v)
		}
	}
}

func (s *Student) PrintBrief(n int) {
	fmt.Printf("%v. %v %v %v (%v)\n", n, s.lastName, s.firstName, s.middleName, s.group)
}

func (s *Student) IsExcellent() bool {
	if len(s.marks) == 0 {
		return false
	}

	excellent := true
	for _, v := range s.marks {
		if v != 5 {
			excellent = false
			break
		}
	}

	return excellent
}

func (s *Student) IsBad() bool {
	bad := false
	for _, v := range s.marks {
		if v < 3 {
			bad = true
			break
		}
	}

	return bad
}

func (s *Student) String() string {
	return fmt.Sprintf("%v %v %v (%v)", s.lastName, s.firstName, s.middleName, s.group)
}
