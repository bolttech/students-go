package main

func main() {
	students := make([]Student, 0)

	err := loadData(&students)
	if err != nil {
		panic(err)
	}

	mainMenuLoop(&students)
}
